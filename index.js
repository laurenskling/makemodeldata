const fs = require('fs');

const dirName = __dirname + '/data';
const make = process.env.MAKE;
if (!make) {
  throw new Error('Missing MAKE in env');
}
const model = process.env.MODEL;
if (!model) {
  throw new Error('Missing MODEL in env');
}
const kind = process.env.KIND;
if (!kind) {
  throw new Error('Missing KIND in env');
}
const fileName = `${dirName}/${kind}.json`;

// checks
try {
  fs.accessSync(dirName);
} catch (err) {
  console.error('Couldn\'t access dir', dirName);
  console.error(err);
  throw err;
}

async function readFile() {
  if (!fs.existsSync(fileName)) {
    return {};
  }
  const file = await fs.readFileSync(fileName, 'utf8');
  return JSON.parse(file);
}

async function writeFile(obj) {
  return fs.writeFileSync(fileName, JSON.stringify(obj), 'utf8');
}

async function run() {
  // get the file
  let file = {};
  try {
    file = await readFile();
  } catch (err) {
    console.error('Couldn\'t read file');
    console.error(err);
    throw err;
  }

  // merge in the values
  const obj = {
    ...file,
    [make]: Array.from(new Set([].concat(file[make] || []).concat(model))).sort(),
  };

  // write the new file
  await writeFile(obj);
}

run();
